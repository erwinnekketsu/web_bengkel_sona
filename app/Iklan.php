<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iklan extends Model
{
    protected $table = 'iklans';

    protected $fillable = ['name', 'image_url'];

    protected $hidden = ["created_at", "updated_at"];
}
