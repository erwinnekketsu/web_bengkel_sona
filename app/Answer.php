<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';

    protected $fillable = ['ask_id', 'user_id', 'answer'];

    function ask()
    {
        return $this->belongsTo('App\Ask');
    }

    function user()
    {
        return $this->belongsTo('App\User');
    }
}
