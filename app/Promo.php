<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    protected $table = 'promos';

    protected $fillable = ['name', 'image_url'];

    protected $hidden = ["created_at", "updated_at"];
}
