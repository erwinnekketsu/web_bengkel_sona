<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ask extends Model
{
    protected $table = 'asks';

    protected $fillable = ['user_id', 'ask', 'seen', 'answers_total'];
}
