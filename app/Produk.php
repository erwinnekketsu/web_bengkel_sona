<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produks';

    protected $fillable = ['name', 'price', 'image'];

    protected $hidden = ["created_at", "updated_at"];

    public function produk_link()
    {
        return $this->hasOne('App\Produk_link');
    }
}
