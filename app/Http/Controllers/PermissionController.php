<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PermissionController extends Controller
{

    public function Permission()
    {
        $admin_permission = Permission::where('slug', 'login')->first();

        //RoleTableSeeder.php
        $dev_role = new Role();
        $dev_role->slug = 'administrator';
        $dev_role->name = 'Administrator';
        $dev_role->save();
        $dev_role->permissions()->attach($admin_permission);

        $manager_role = new Role();
        $manager_role->slug = 'user';
        $manager_role->name = 'User';
        $manager_role->save();

        $dev_role = Role::where('slug', 'administrator')->first();

        $createTasks = new Permission();
        $createTasks->slug = 'login';
        $createTasks->name = 'Login';
        $createTasks->save();
        $createTasks->roles()->attach($dev_role);

        // $developer = new User();
        // $developer->name = 'Harsukh Makwana';
        // $developer->username = 'harsukh';
        // $developer->email = 'harsukh21@gmail.com';
        // $developer->password = bcrypt('password');
        // $developer->api_token = Str::random(40);
        // $developer->save();

        // $manager = new User();
        // $manager->name = 'Jitesh Meniya';
        // $manager->username = 'jitesh';
        // $manager->email = 'jitesh21@gmail.com';
        // $manager->password = bcrypt('password');
        // $manager->api_token = Str::random(40);
        // $manager->save();


        return redirect()->back();
    }
}