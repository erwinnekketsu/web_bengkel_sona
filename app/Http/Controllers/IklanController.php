<?php

namespace App\Http\Controllers;

use App\Iklan;
use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;

class IklanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Iklan::paginate(10);

        return view('iklan/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('iklan/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
        ]);

        $image = $request->file('image')->getRealPath();

        Cloudder::upload($image, null, array("format" => "jpg"));

        $result = \Cloudder::getResult();
        if ($result) {

            $image_url = Cloudder::secureShow(Cloudder::getPublicId());

            Iklan::create([
                'name' => $request->name,
                'image_url' => $image_url
            ]);

            return redirect('/iklan')->with('success', 'You have successfully add iklans.');
        }

        Redirect::back()->withInput(Input::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Iklan  $iklan
     * @return \Illuminate\Http\Response
     */
    public function show(Iklan $iklan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Iklan  $iklan
     * @return \Illuminate\Http\Response
     */
    public function edit(Iklan $iklan)
    {
        $data = Iklan::findOrFail($iklan->id);

        return view('iklan/edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Iklan  $iklan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $iklan = Iklan::find($id);

        $this->validate($request, [
            'name' => 'required',
        ]);

        if ($request->file()) {
            $this->validate($request, [
                'image' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
            ]);

            $image = $request->file('image')->getRealPath();

            Cloudder::upload($image, null, array("format" => "jpg"));

            $result = \Cloudder::getResult();
            if ($result) {

                $image_url = Cloudder::secureShow(Cloudder::getPublicId());

                $iklan->image_url = $image_url;
            }
        }

        $iklan->name = $request->get('name');

        $iklan->save();

        return redirect('/iklan')->with('success', 'You have successfully edit iklans.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Iklan  $iklan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $iklan = Iklan::find($id);
        $publicId = pathinfo($iklan->image_url)['filename'];
        Cloudder::delete($publicId);
        $iklan->delete();
  
        return redirect()->route('iklan.index')->with('success','Deleted successfully');
    }
}
