<?php

namespace App\Http\Controllers;

use App\Produk;
use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Produk::paginate(10);
        
        return view('produk/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produk/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'image' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
        ]);

        $image = $request->file('image')->getRealPath();

        Cloudder::upload($image, null, array("format" => "jpg"));

        $result = \Cloudder::getResult();
        if ($result) {

            $image_url = Cloudder::secureShow(Cloudder::getPublicId(), ["width" =>  null, "height" => null, "crop" => null]);
    
            Produk::create([
                'name' => $request->name,
                'price' => $request->price,
                'image' => $image_url
            ]);
    
            return redirect('/produk')->with('success', 'You have successfully add products.');
        }

        Redirect::back()->withInput(Input::all());

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit(Produk $produk)
    {
        $data = Produk::findOrFail($produk->id);

        return view('produk/edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produk = Produk::find($id);

        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
        ]);

        if ($request->file()) {
            $this->validate($request, [
                'image' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
            ]);

            $image = $request->file('image')->getRealPath();

            Cloudder::upload($image, null, array("format" => "jpg"));

            $result = \Cloudder::getResult();
            if ($result) {

                $image_url = Cloudder::secureShow(Cloudder::getPublicId(), ["width" =>  null, "height" => null, "crop" => null]);

                $produk->image = $image_url;

            }
        }
        
        $produk->name = $request->get('name');
        $produk->price = $request->get('price');

        $produk->save();

        return redirect('/produk')->with('success', 'You have successfully edit products.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);
        $publicId = pathinfo($produk->image)['filename'];
        Cloudder::delete($publicId);
        $produk->delete();

        return redirect()->route('produk.index')->with('success', 'Deleted successfully');
    }
}
