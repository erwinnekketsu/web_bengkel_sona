<?php

namespace App\Http\Controllers\API;

use App\Promo;
use App\Http\Controllers\Controller;

class PromoController extends Controller
{
    public function index()
    {
        $data = Promo::paginate(10);

        return response()->json($data);
    }
}
