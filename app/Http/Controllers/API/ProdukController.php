<?php

namespace App\Http\Controllers\API;

use App\Produk;
use App\Http\Controllers\Controller;

class ProdukController extends Controller
{
    public function index()
    {
        $data = Produk::paginate(10);

        return response()->json($data);
    }
}
