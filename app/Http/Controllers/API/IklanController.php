<?php

namespace App\Http\Controllers\API;

use App\Iklan;
use App\Http\Controllers\Controller;

class IklanController extends Controller
{
    public function index()
    {
        $data = Iklan::paginate(10);

        return response()->json($data);
    }
}
