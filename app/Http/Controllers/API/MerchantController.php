<?php

namespace App\Http\Controllers\API;

use App\Merchant;
use App\Http\Controllers\Controller;

class MerchantController extends Controller
{
    public function index()
    {
        $data = Merchant::paginate(10);

        return response()->json($data);
    }
}
