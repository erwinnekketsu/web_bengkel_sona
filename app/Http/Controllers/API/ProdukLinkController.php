<?php

namespace App\Http\Controllers\API;

use App\Produk;
use App\Produk_link;
use App\Http\Controllers\Controller;

class ProdukLinkController extends Controller
{
    public function index($id)
    {
        $data = Produk::find($id);
        $link = Produk_link::where('produk_id', $id)->get();

        $data->link = $link;

        return response()->json($data);
    }
}
