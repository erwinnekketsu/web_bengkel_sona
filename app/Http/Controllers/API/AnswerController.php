<?php

namespace App\Http\Controllers\API;

use App\Ask;
use App\Answer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AnswerController extends Controller
{
    public function index($id)
    {
        $data = Ask::find($id);
        $answer = Answer::where('ask_id', $id)->get();

        $data->answer = $answer;

        return response()->json($data);
    }

    public function store(Request $request, $ask_id)
    {
        $validator = Validator::make($request->all(), [
            'answer' => 'required'
        ]);

        $errors = $validator->errors();

        if ($errors->first()) {
            return response(
                ['status' => 'failed', 'message' => $errors->first()],
                400
            );
        }

        Answer::create([
            'ask_id' => $ask_id,
            'user_id' => $request->user()->id,
            'answer' => $request->answer
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Create succesfully',
        ]);
    }
}
