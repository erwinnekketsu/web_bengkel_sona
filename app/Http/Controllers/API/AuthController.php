<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use App\Permission;
use App\Role;

class AuthController extends Controller
{
    protected $api_token;

    use SendsPasswordResetEmails;

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $errors = $validator->errors();

        if ($errors->first()) {
            return response(
                ['status' => 'failed', 'message' => $errors->first()],
                400
            );
        }

        $user = User::where('email', $request->email)->first();
        
        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()->json(['status' => 'failed', 'message' => 'The email or password is incorrect.']);
        }

        if ($user && !$user->email_verified_at) {
            return response()->json(['status' => 'failed', 'message' => 'User not verified!']);
        }

        if ($user && $user->active != 1) {
            return response()->json(['status' => 'failed', 'message' => 'Your session has expired because your account is deactivated!']);
        }

        $this->api_token = Str::random(40);

        $user->update(['api_token' => $this->api_token, 'api_token_expires' => strtotime("+1 days")]);

        return response()->json([
            'status' => 'success',
            'message' => 'User login succesfully',
            'token' => $this->api_token,
            'token_expires' => $user->api_token_expires,
            'user' => $user
        ]);
    }

    public function register(Request $request)
    {
        $requestData = $request->all();
        
        $validator =  Validator::make($requestData, [
            'name' => 'required|string|max:64',
            'email' => 'required|string|email|max:64',
            'username' => 'required|string|max:64',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator);
        }

        $user = User::create([
            'name' => $requestData['name'],
            'email' => $requestData['email'],
            'username' => $requestData['username'],
            'password' => bcrypt($requestData['password']),
            'api_token' => Str::random(40)
        ]);

        $role = Role::where('slug', 'User')->first();

        $user->roles()->attach($role);

        $user->sendEmailVerificationNotification();

        return response()->json([
            'status' => 'success',
            'msg' => 'User created, We will send you an email to verify. If you dont see the verification email in your inbox, please check your Junk or Spam folders'
        ]);

    }

    public function resetPassword(Request $request)
    {
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        if ($response == Password::RESET_LINK_SENT) {
            return response()->json([
                'status' => 'Success',
                'msg' => 'We have e-mailed your password reset link!'
            ]);
        }

        return response()->json([
            'status' => "Failed",
            'msg' => "We can't find a user with that e-mail address."
        ]);
    }
}
