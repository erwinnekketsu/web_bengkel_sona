<?php

namespace App\Http\Controllers\API;

use DB;
use App\Ask;
use App\Answer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AskController extends Controller
{
    public function index()
    {
        $data = Ask::select('asks.*', DB::raw('IFNULL( answers.total_reply, 0) as total_reply'))
            ->leftJoin(DB::raw('(SELECT ask_id, COUNT( ask_id ) AS total_reply FROM answers GROUP BY ask_id) as answers'), 'asks.id', '=', DB::raw('answers.ask_id'))
            ->paginate(10);

        return response()->json($data);
    }

    public function show($id)
    {
        $data = Ask::find($id);
        $answer = Answer::where('ask_id', $id);

        $data->total_reply = $answer->count();
        $data->answer = $answer->get();

        Ask::where('id', $id)->increment('seen');

        return response()->json($data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ask' => 'required',
        ]);

        $errors = $validator->errors();

        if ($errors->first()) {
            return response(
                ['status' => 'failed', 'message' => $errors->first()],
                400
            );
        }
        Ask::create([
            'user_id' => $request->user()->id,
            'ask' => $request->ask,
            'seen' => 0,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Create succesfully',
        ]);
    }
}
