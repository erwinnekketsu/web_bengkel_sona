<?php

namespace App\Http\Controllers;

use App\Promo;
use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Promo::paginate(10);

        return view('promo/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('promo/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
        ]);

        $image = $request->file('image')->getRealPath();

        Cloudder::upload($image, null, array("format" => "jpg"));

        $result = \Cloudder::getResult();
        if ($result) {

            $image_url = Cloudder::secureShow(Cloudder::getPublicId());

            Promo::create([
                'name' => $request->name,
                'image_url' => $image_url
            ]);

            return redirect('/promo')->with('success', 'You have successfully add promos.');
        }

        Redirect::back()->withInput(Input::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function show(Promo $promo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function edit(Promo $promo)
    {
        $data = Promo::findOrFail($promo->id);

        return view('promo/edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $promo = Promo::find($id);

        $this->validate($request, [
            'name' => 'required',
        ]);

        if ($request->file()) {
            $this->validate($request, [
                'image' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
            ]);

            $image = $request->file('image')->getRealPath();

            Cloudder::upload($image, null, array("format" => "jpg"));

            $result = \Cloudder::getResult();
            if ($result) {

                $image_url = Cloudder::secureShow(Cloudder::getPublicId());

                $promo->image_url = $image_url;
            }
        }

        $promo->name = $request->get('name');

        $promo->save();

        return redirect('/promo')->with('success', 'You have successfully edit promos.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $promo = Promo::find($id);
        $publicId = pathinfo($promo->image_url)['filename'];
        Cloudder::delete($publicId);
        $promo->delete();

        return redirect()->route('promo.index')->with('success', 'Deleted successfully');
    }
}
