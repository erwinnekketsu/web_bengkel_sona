<?php

namespace App\Http\Controllers;

use App\Merchant;
use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;

class MerchantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Merchant::paginate(10);

        return view('merchant/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('merchant/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
            'image' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
        ]);

        $image = $request->file('image')->getRealPath();

        Cloudder::upload($image, null, array("format" => "jpg"));

        $result = \Cloudder::getResult();
        if ($result) {

            $image_url = Cloudder::secureShow(Cloudder::getPublicId());

            Merchant::create([
                'name' => $request->name,
                'address' => $request->address,
                'longitude' => $request->longitude,
                'latitude' => $request->latitude,
                'foto' => $image_url
            ]);

            return redirect('/merchant')->with('success', 'You have successfully add merchants.');
        }

        Redirect::back()->withInput(Input::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function show(Merchant $merchant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function edit(Merchant $merchant)
    {
        $data = Merchant::findOrFail($merchant->id);

        return view('merchant/edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $merchant = Merchant::find($id);

        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'longitude' => 'required',
            'latitude' => 'required'
        ]);

        if ($request->file()) {
            $this->validate($request, [
                'image' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
            ]);

            $image = $request->file('image')->getRealPath();

            Cloudder::upload($image, null, array("format" => "jpg", "width" =>  null, "height" => null, "crop" => null));

            $result = \Cloudder::getResult();
            if ($result) {

                $image_url = Cloudder::secureShow(Cloudder::getPublicId());

                $merchant->image_url = $image_url;
            }
        }

        $merchant->name = $request->get('name');
        $merchant->address = $request->get('address');
        $merchant->longitude = $request->get('longitude');
        $merchant->latitude = $request->get('latitude');

        $merchant->save();

        return redirect('/merchant')->with('success', 'You have successfully edit merchants.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Merchant  $merchant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $merchant = Merchant::find($id);
        $publicId = pathinfo($merchant->foto)['filename'];
        Cloudder::delete($publicId);
        $merchant->delete();
  
        return redirect()->route('merchant.index')->with('success','Deleted successfully');
    }
}
