<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::paginate(10);

        return view('user/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request)
    {

        $user = User::find($request->id);

        // Make sure you've got the Page model
        if ($user) {
            $user->email_verified_at = Carbon::now();
            $user->save();

            return response()->json(['success' => 'Success'], 200); // Status code here
        }
        return response()->json(['error' => 'Error'], 500); // Status code here
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request)
    {

        $user = User::find($request->id);

        // Make sure you've got the Page model
        if ($user) {
            $user->active = 1;
            $user->save();

            return response()->json(['success' => 'Success'], 200); // Status code here
        }
        return response()->json(['error' => 'Error'], 500); // Status code here
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deactive(Request $request)
    {

        $user = User::find($request->id);

        // Make sure you've got the Page model
        if ($user) {
            $user->active = 0;
            $user->save();

            return response()->json(['success' => 'Success'], 200); // Status code here
        }
        return response()->json(['error' => 'Error'], 500); // Status code here
    }
}
