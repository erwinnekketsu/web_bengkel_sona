<?php

namespace App\Http\Controllers;

use App\Produk_link;
use App\Produk;
use Illuminate\Http\Request;

class ProdukLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Produk_link::select('produk_links.*', 'produks.name as produk')->leftJoin('produks', 'produks.id', '=', 'produk_links.produk_id')->paginate(10);
        
        return view('produk_link/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Produk::pluck('name', 'id');
        $data->prepend('-- Pilih Produk --', 0);

        return view('produk_link/create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'produk_id' => 'required',
            'name' => 'required',
            'link' => 'required'
        ]);

        Produk_link::create([
            'produk_id' => $request->produk_id,
            'name' => $request->name,
            'link' => $request->link
        ]);

        return redirect('/produk-link');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produk_link  $produk_link
     * @return \Illuminate\Http\Response
     */
    public function show(Produk_link $produk_link)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produk_link  $produk_link
     * @return \Illuminate\Http\Response
     */
    public function edit(Produk_link $produk_link)
    {
        $produk = Produk::pluck('name', 'id');
        $produk->prepend('-- Pilih Produk --', 0);

        $data = Produk_link::findOrFail($produk_link->id);

        return view('produk_link/edit', compact('produk', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produk_link  $produk_link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'produk_id' => 'required',
            'name' => 'required',
            'link' => 'required'
        ]);

        $produk = Produk_link::find($id);

        $produk->produk_id = $request->get('produk_id');
        $produk->name = $request->get('name');
        $produk->link = $request->get('link');

        $produk->save();

        return redirect('/produk-link');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produk_link  $produk_link
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk_link = Produk_link::find($id);
        $produk_link->delete();

        return redirect()->route('produk-link.index')->with('success', 'Deleted successfully');
    }
}
