<?php

namespace App\Http\Controllers;

use App\Ask;
use App\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class AskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Ask::leftJoin('users', 'users.id', '=', 'asks.user_id')
                    ->leftJoin('answers', 'answers.ask_id', '=', 'asks.id')
                    ->select(
                        DB::RAW("max(asks.id) as id"),
                        DB::RAW("max(users.name) as name"),
                        DB::RAW("max(asks.ask) as ask"),
                        DB::RAW("max(asks.seen) as seen"),
                        DB::RAW("count(answers.id) as answers_total")
                    )
                    ->groupBy('asks.id')
                    ->orderBy('asks.id')
                    ->paginate(10);
        
        return view('ask/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ask/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ask' => 'required',
        ]);

        Ask::create([
            'user_id' => Auth::id(),
            'ask' => $request->ask,
            'seen' => 0,
        ]);

        return redirect('/ask');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ask  $ask
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ask = Ask::leftJoin('users', 'users.id', '=', 'asks.user_id')
                    ->where('asks.id', $id)->first();

        $answer = Answer::leftJoin('asks', 'asks.id', '=', 'answers.ask_id')
                        ->leftJoin('users', 'users.id', '=', 'answers.user_id')
                        ->where('ask_id', $id)->get();

        Ask::where('id', $id)->increment('seen'); 

        return view('ask/show', compact('ask', 'answer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ask  $ask
     * @return \Illuminate\Http\Response
     */
    public function edit(Ask $ask)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ask  $ask
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ask $ask)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ask  $ask
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ask = Ask::find($id);
        $ask->delete();

        return redirect()->route('ask.index')->with('success', 'Deleted successfully');
    }
}
