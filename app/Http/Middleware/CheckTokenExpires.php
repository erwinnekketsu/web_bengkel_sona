<?php

namespace App\Http\Middleware;

use Closure;

class CheckTokenExpires
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->api_token_expires < strtotime("now")) {
            return response()->json([
                'status' => 'expired',
                'message' => 'The access token expired'
            ], 401);
        }

        if ($request->user()->active == 0) {
            return response()->json([
                'status' => 'deactive',
                'message' => 'Your account has been deactivated. Please contact administrator!'
            ], 401);
        }

        $this->tokenExpiration($request);

        return $next($request);
    }

    public function tokenExpiration($request)
    {   
        \App\User::where('id', $request->user()->id)->update(['api_token_expires' => strtotime("now, +1 days")]);
    }
}
