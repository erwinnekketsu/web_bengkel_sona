<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    protected $table = 'merchants';

    protected $fillable = ['name', 'address', 'foto', 'longitude', 'latitude'];

    protected $hidden = ["created_at", "updated_at"];
}
