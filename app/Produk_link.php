<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk_link extends Model
{
    protected $table = 'produk_links';

    protected $fillable = ['produk_id', 'link', 'name'];

    protected $hidden = ["created_at", "updated_at"];

    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }
}
