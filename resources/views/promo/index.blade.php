@extends('layouts.default')

@section('content')
<div class="container-fluid">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">Promo</h4>
                <a onclick="showModal('/promo/create')" class="btn btn-primary">Tambah</a>
            </div>
            <div class="card-content table-responsive">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <table class="table table-hover">
                    <thead class="text-success">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Image</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @forelse ($data as $key=>$item)
                            <tr>
                                <td>{{(($data->currentPage() - 1 ) * $data->perPage() ) + $loop->iteration}}</td>
                                <td>{{$item->name}}</td>
                                <td>
                                    @if ($item->image_url)
                                        <img src="{{$item->image_url}}" width="100px" height="100px">    
                                    @else
                                        <img src="https://redzonekickboxing.com/wp-content/uploads/2017/04/default-image-620x600.jpg" width="100px" height="100px">
                                    @endif
                                </td>
                                <td><a onclick="showModal('/promo/{{$item->id}}/edit')">Edit</a>
                                    <a onclick="deleteData('/delete/promo/{{$item->id}}')">Delete</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Tidak ada data.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                {{ $data->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
