@extends('layouts.default')

@section('content')
<div class="container-fluid">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">Partner</h4>
                <a onclick="showModal('/partner/create')" class="btn btn-primary">Tambah</a>
            </div>
            <div class="card-content table-responsive">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <table class="table table-hover">
                    <thead class="text-success">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Address</th>
                        <th>Foto</th>
                        <th>Longitude</th>
                        <th>Latitude</th>
                    </thead>
                    <tbody>
                        @forelse ($data as $key=>$item)
                            <tr>
                                <td>{{(($data->currentPage() - 1 ) * $data->perPage() ) + $loop->iteration}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->address}}</td>
                                <td>
                                    @if ($item->foto)
                                    <img src="{{$item->foto}}" width="100px" height="100px">    
                                    @else
                                    <img src="https://redzonekickboxing.com/wp-content/uploads/2017/04/default-image-620x600.jpg" width="100px" height="100px">
                                    @endif
                                </td>
                                <td>{{$item->longitude}}</td>
                                <td>{{$item->latitude}}</td>
                                <td><a onclick="showModal('/partner/{{$item->id}}/edit')">Edit</a>
                    
                                    <a onclick="deleteData('/delete/partner/{{$item->id}}')">Delete</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">Tidak ada data.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                {{ $data->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
<!-- Push a style dynamically from a view -->
@push('styles')
    <!-- Load Leaflet Map Css from CDN -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" />

    <!-- Load Esri Leaflet Geocoder from CDN -->
    <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css" integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g==" crossorigin="">
@endpush

<!-- Push a script dynamically from a view -->
@push('scripts')
    <!-- Load Leaflet Map Js from CDN -->
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
    <!-- Load Plugin Esri Leaflet map from CDN -->
    <script src="https://unpkg.com/esri-leaflet@2.4.1/dist/esri-leaflet.js" integrity="sha512-xY2smLIHKirD03vHKDJ2u4pqeHA7OQZZ27EjtqmuhDguxiUvdsOuXMwkg16PQrm9cgTmXtoxA6kwr8KBy3cdcw==" crossorigin=""></script>
    <!-- Load Plugin Esri Leaflet Geocoder map from CDN -->
    <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js" integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA==" crossorigin=""></script>
@endpush
