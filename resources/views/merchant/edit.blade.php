{{-- @extends('layouts.default') --}}

{{-- @section('content') --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="green">
                    <h4 class="title">Edit Partner</h4>
                </div>
                <div class="card-content">
                    <form method="post" action="/partner/{{$data->id}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nama</label>
                                    <input type="text" name="name" class="form-control" value="{{$data->name}}">
                                    @if($errors->has('name'))
                                        <span class="help-block">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Alamat</label>
                                    <textarea type="text" name="address" id="addressInput" class="form-control" >{{$data->address}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group row">
                                <label for="map" class="col-sm-2 col-form-label">Pilih Lokasi</label>
                                <div class="col-md-10">
                                    <div id="map"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Longitude</label>
                                    <input type="text" name="longitude" id="longitude" class="form-control" value="{{$data->longitude}}">
                                    @if($errors->has('longitude'))
                                        <span class="help-block">{{ $errors->first('longitude') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Latitude</label>
                                    <input type="text" name="latitude" id="latitude" class="form-control" value="{{$data->latitude}}">
                                    @if($errors->has('latitude'))
                                        <span class="help-block">{{ $errors->first('latitude') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group label-floating mb-3 px-2 py-2 rounded-pill bg-white shadow-sm">
                                    <input id="upload" type="file" name="image" class="form-control border-0">
                                    <label id="upload-label" for="upload" class="font-weight-light ">Foto</label>
                                    <div class="input-group-append">
                                        <label for="upload" class="btn btn-light m-0 rounded-pill px-4"> <i class="fa fa-cloud-upload mr-2 "></i><small class="text-uppercase font-weight-bold "> Choose file</small></label>
                                    </div>
                                </div>
                                @if($errors->has('image'))
                                    <span class="help-block">{{ $errors->first('image') }}</span>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    let facility_latitude = parseFloat("{{$data->latitude}}") || null
    let facility_longitude = parseFloat("{{$data->longitude}}") || null
</script>
<script src="{{ asset('js/map-edit.js') }}"></script>
{{-- @endsection --}}
