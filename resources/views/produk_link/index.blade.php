@extends('layouts.default')

@section('content')
<div class="container-fluid">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">Produk</h4>
                <a onclick="showModal('/produk-link/create')" class="btn btn-primary">Tambah</a>
            </div>
            <div class="card-content table-responsive">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <table class="table table-hover">
                    <thead class="text-success">
                        <th>No</th>
                        <th>Name</th>
                        <th>Produk</th>
                        <th>Link</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @forelse ($data as $key=>$item)
                            <tr>
                                <td>{{(($data->currentPage() - 1 ) * $data->perPage() ) + $loop->iteration}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->produk}}</td>
                                <td>{{$item->link}}</td>
                                <td><a onclick="showModal('/produk-link/{{$item->id}}/edit')">Edit</a>
                                    <a onclick="deleteData('/delete/produk-link/{{$item->id}}')">Delete</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Tidak ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                {{ $data->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
