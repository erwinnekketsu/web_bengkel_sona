<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Bengkel</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.css" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.js"></script>
    @stack('styles')
</head>

<body>
    <div class="wrapper">
      <div class="sidebar" data-color="green">
          <div class="logo">
              <a href="/" class="simple-text">
                  Dashboard
              </a>
          </div>
          <div class="sidebar-wrapper">
              <ul class="nav">
                  <li>
                      <a href="/user">
                          <i class="material-icons">content_paste</i>
                          <p>User</p>
                      </a>
                  </li>
                  <li>
                      <a href="/produk">
                          <i class="material-icons">content_paste</i>
                          <p>Produk</p>
                      </a>
                  </li>
                  <li>
                      <a href="/produk-link">
                          <i class="material-icons">content_paste</i>
                          <p>Produk Link</p>
                      </a>
                  </li>
                  <li>
                      <a href="/ask">
                          <i class="material-icons">content_paste</i>
                          <p>Ask</p>
                      </a>
                  <li>
                      <a href="/answer">
                          <i class="material-icons">content_paste</i>
                          <p>Answer</p>
                      </a>
                  </li>
                  <li>
                      <a href="/promo">
                          <i class="material-icons">content_paste</i>
                          <p>Promo</p>
                      </a>
                  </li>
                  <li>
                      <a href="/iklan">
                          <i class="material-icons">content_paste</i>
                          <p>Iklan</p>
                      </a>
                  </li>
                  <li>
                      <a href="/partner">
                          <i class="material-icons">content_paste</i>
                          <p>Partner</p>
                      </a>
                  </li>
              </ul>
          </div>
      </div>
        <div class="main-panel">
          <nav class="navbar navbar-transparent navbar-absolute">
              <div class="container-fluid">
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </button>
                  </div>
                  <div class="collapse navbar-collapse">
                      <ul class="nav navbar-nav navbar-right">
                          {{-- <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                  <i class="material-icons">notifications</i>
                                  <span class="notification">5</span>
                                  <p class="hidden-lg hidden-md">Notifications</p>
                              </a> 
                                <ul class="dropdown-menu">
                                  <li>
                                      <a href="#">Mike John responded to your email</a>
                                  </li>
                                  <li>
                                      <a href="#">You have 5 new tasks</a>
                                  </li>
                                  <li>
                                      <a href="#">You're now friend with Andrew</a>
                                  </li>
                                  <li>
                                      <a href="#">Another Notification</a>
                                  </li>
                                  <li>
                                      <a href="#">Another One</a>
                                  </li>
                              </ul> 
                          </li>--}}
                          <li>
                              <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();" title="Logout">
                                  <i class="fa fa-sign-out" aria-hidden="true"></i>
                                  <p class="hidden-lg hidden-md">Profile</p>
                              </a>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>
            <div class="content">
              <!-- content -->
              @yield('content')
            </div>
            <footer class="footer">
                {{-- <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                    </p> --}}
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/material.min.js') }}" type="text/javascript"></script>
<script src="https://momentjs.com/downloads/moment.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="{{ asset('js/chartist.min.js') }}"></script>
<!--  Dynamic Elements plugin -->
<script src="{{ asset('js/arrive.min.js') }}"></script>
<!--  PerfectScrollbar Library -->
<script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="{{ asset('js/bootstrap-notify.js') }}"></script>
<!-- Material Dashboard javascript methods -->
<script src="{{ asset('js/material-dashboard.js?v=1.2.0') }}"></script>
<!-- sweet alert -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<!-- Custom javascript methods -->
<script src="{{ asset('js/custom.js?v=1.0') }}"></script>

@stack('scripts')

<script>
$(function(){
    var url = window.location.pathname, 
    urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
    // now grab every link from the navigation
    console.log(url);
    $('.sidebar-wrapper a').each(function(){
        // and test its normalized href against the url pathname regexp
        if(urlRegExp.test(this.href.replace(/\/$/,''))){
            $(this).parent('li').addClass('active');
        }
    });
});
</script>
</html>
