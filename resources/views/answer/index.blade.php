@extends('layouts.default')

@section('content')
<div class="container-fluid">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">Answer</h4>
                <a onclick="showModal('/answer/create')" class="btn btn-primary">Add</a>
            </div>
            <div class="card-content table-responsive">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <table class="table table-hover">
                    <thead class="text-success">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Ask</th>
                        <th>Answer</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @forelse ($data as $key=>$item)
                            <tr>
                                <td>{{(($data->currentPage() - 1 ) * $data->perPage() ) + $loop->iteration}}</td>
                                <td>{{$item->user->name}}</td>
                                <td>{{$item->ask->ask}}</td>
                                <td>{{$item->answer}}</td>
                                <td><a onclick="showModal('/answer/{{$item->id}}/edit')">Edit</a>
                                    <a onclick="deleteData('/delete/answer/{{$item->id}}')">Delete</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">Tidak ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                {{ $data->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
