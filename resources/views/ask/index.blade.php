@extends('layouts.default')

@section('content')
<div class="container-fluid">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">Ask</h4>
                <a onclick="showModal('/ask/create')" class="btn btn-primary">Add</a>
            </div>
            <div class="card-content table-responsive">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <table class="table table-hover">
                    <thead class="text-success">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Ask</th>
                        <th>Seen</th>
                        <th>Answers Total</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @forelse ($data as $key=>$item)
                            <tr>
                                <td>{{(($data->currentPage() - 1 ) * $data->perPage() ) + $loop->iteration}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->ask}}</td>
                                <td>{{$item->seen}}</td>
                                <td><a onclick="showModal('/ask/{{$item->id}}')" style="cursor: pointer">{{$item->answers_total}}</a></td>
                                <td><a onclick="showModal('/ask/{{$item->id}}/edit')">Edit</a>
                                    <a onclick="deleteData('/delete/ask/{{$item->id}}')">Delete</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">Tidak ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                {{ $data->links() }}
            </div>
        </div>
    </div>
</div>
@endsection