<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-content">
                    <blockquote>
                        {{$ask->ask}}
                    </blockquote>
                    @forelse ($answer as $item)
                        <blockquote class="blockquote-reverse">
                            <p>{{$item->answer}}</p>
                            <footer>{{$item->created_at}}</footer>
                        </blockquote>
                    @empty
                        <blockquote class="blockquote-reverse">
                            <p>Tidak ada jawaban.</p>
                        </blockquote>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
