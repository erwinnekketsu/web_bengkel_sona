@extends('layouts.default')

@section('content')
<div class="container-fluid">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">User</h4>
            </div>
            <div class="card-content table-responsive">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <table class="table table-hover">
                    <thead class="text-success">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Created at</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @forelse ($data as $key=>$item)
                            <tr>
                                <td>{{(($data->currentPage() - 1 ) * $data->perPage() ) + $loop->iteration}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->created_at->format('d F Y')}}</td>
                                <td>
                                @if (is_null($item->email_verified_at))
                                    <a onclick="verifyUser({{$item->id}})">Verify</a>
                                @else
                                    @if ($item->active)
                                        <a onclick="deactiveUser({{$item->id}})">Active</a>
                                    @else
                                        <a onclick="activeUser({{$item->id}})">Deactive</a>
                                    @endif
                                @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">Tidak ada data.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                {{ $data->links() }}
            </div>
        </div>
    </div>
</div>
@endsection