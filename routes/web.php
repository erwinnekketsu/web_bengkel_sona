<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);
Auth::routes();

Route::get('/home/resend', 'HomeController@resend')->name('home.resend');
Route::get('/email-verified', 'HomeController@email')->name('email-verified');

//home
Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

/* All Authenticated routes here */
Route::middleware(['auth', 'verified', 'role:administrator'])->group(function () {
    //dashboard
    Route::get('/', 'DashboardController@index')->name('dashboard');

    //produk
    Route::resource('produk', 'ProdukController');
    Route::get('delete/produk/{id}', 'ProdukController@destroy');

    //produk_link
    Route::resource('produk-link', 'ProdukLinkController');
    Route::get('delete/produk-link/{id}', 'ProdukLinkController@destroy');

    //ask
    Route::resource('ask', 'AskController');
    Route::get('delete/ask/{id}', 'AskController@destroy');

    //ask
    Route::resource('answer', 'AnswerController');
    Route::get('/answer/create/{id}', 'AnswerController@create');
    Route::get('delete/answer/{id}', 'AnswerController@destroy');

    //promo
    Route::resource('promo', 'PromoController');
    Route::get('delete/promo/{id}', 'PromoController@destroy');

    //iklan
    Route::resource('iklan', 'IklanController');
    Route::get('delete/iklan/{id}', 'IklanController@destroy');

    //merchant
    Route::resource('merchant', 'MerchantController');
    Route::get('delete/merchant/{id}', 'MerchantController@destroy');
    Route::resource('partner', 'MerchantController');
    Route::get('delete/partner/{id}', 'MerchantController@destroy');
    
    //user
    Route::resource('user', 'UserController');
    Route::post('user/verify', 'UserController@verify');
    Route::post('user/active', 'UserController@active');
    Route::post('user/deactive', 'UserController@deactive');
});