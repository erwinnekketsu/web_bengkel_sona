<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Auth::routes(['verify' => true]);

Route::prefix('v1')->group(function () {

    Route::post('/login', 'API\AuthController@login');
    Route::post('/register', 'API\AuthController@register');
    Route::post('/password/reset', 'API\AuthController@resetPassword');

    /* All Authenticated routes here */
    Route::middleware(['auth:api', 'verified', 'check-token'])->group(function () {

        Route::get('/produk', 'API\ProdukController@index');

        Route::get('/produk-link/{id}', 'API\ProdukLinkController@index');
        
        Route::get('/ask', 'API\AskController@index');
        Route::get('/ask/{id}', 'API\AskController@show');
        Route::post('/ask', 'API\AskController@store');
        
        Route::get('/answer/{id}', 'API\AnswerController@index');
        Route::post('/answer/{id}', 'API\AnswerController@store');

        Route::get('/promo', 'API\PromoController@index');

        Route::get('/iklan', 'API\IklanController@index');

        Route::get('/partner', 'API\MerchantController@index');

        // Route::middleware('auth:api')->get('/user', function (Request $request) {
        //     return $request->user();
        // });

    });
});