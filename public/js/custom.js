function showModal(uri) {
    var dialog = new BootstrapDialog({
        message: $('<div></div>').load(uri)
    });
    dialog.realize();
    dialog.getModalHeader().hide();
    dialog.getModalFooter().hide();
    dialog.open();
}

function deleteData(path){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            location.href = path;
        }
    })
}

function verifyUser(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "Verify this user!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Verify'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "/user/verify",
                method: "POST",
                data: {
                    _token: $('meta[name="_token"]').attr('content'),
                    id: id
                },
                success: function () {
                    swal.fire("Verified!", "Successfully verified", "success");
                    location.reload();
                },
                error: function () {
                    swal.fire("Failed!", "Please try again", "failed");
                }
            });
        }
    })
}

function activeUser(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "Active this user!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Active'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "/user/active",
                method: "POST",
                data: {
                    _token: $('meta[name="_token"]').attr('content'),
                    id: id
                },
                success: function () {
                    swal.fire("Actived!", "Successfully actived", "success");
                    location.reload();
                },
                error: function () {
                    swal.fire("Failed!", "Please try again", "failed");
                }
            });
        }
    })
}

function deactiveUser(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "Deactive this user!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Deactive'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "/user/deactive",
                method: "POST",
                data: {
                    _token: $('meta[name="_token"]').attr('content'),
                    id: id
                },
                success: function () {
                    swal.fire("Deactived!", "Successfully deactived", "success");
                    location.reload();
                },
                error: function () {
                    swal.fire("Failed!", "Please try again", "failed");
                }
            });
        }
    })
}