$(document).ready(function () {
    var latlng = new L.LatLng(facility_latitude, facility_longitude);
    console.log('latlng init edit', latlng)
    // set view current position
    var map = L.map('map').setView(latlng, 17)

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    var geocodeService = L.esri.Geocoding.geocodeService();
    var searchControl = L.esri.Geocoding.geosearch().addTo(map);

    // set data map in textbox
    document.getElementById('latitude').value = latlng.lat;
    document.getElementById('longitude').value = latlng.lng;
    // document.getElementById('addressInput').value = latlng.address == "undefined" ? "" : latlng.address;

    // set marker on current location
    var marker = L.marker(latlng).addTo(map);

    // create new layer group for save search result
    var results = new L.LayerGroup().addTo(map);

    map.on('click', function (e) {
        // geocodeService.reverse() => to get data location as address latitude and longtitude
        geocodeService.reverse().latlng(e.latlng).run(function (error, result) {

            console.log('click result', result)

            if (error) {
                return;
            }

            // clear marker on map before create marker
            if (marker) {
                results.clearLayers();
                map.removeLayer(marker);
            }

            // to add marker and popup result address
            marker = L.marker(result.latlng).addTo(map).bindPopup(result.address.Match_addr).openPopup();

            document.getElementById('latitude').value = result.latlng.lat;
            document.getElementById('longitude').value = result.latlng.lng;
            document.getElementById('addressInput').value = result.address.Match_addr;
        });
    });

    // event get result from search
    searchControl.on('results', function (data) {
        results.clearLayers();
        for (var i = data.results.length - 1; i >= 0; i--) {
            results.addLayer(L.marker(data.results[i].latlng));
            document.getElementById('latitude').value = data.results[i].latlng.lat;
            document.getElementById('longitude').value = data.results[i].latlng.lng;
            document.getElementById('addressInput').value = data.results[i].properties.Match_addr;
        }
    });
})