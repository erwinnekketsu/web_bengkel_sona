$(document).ready(function () {
    // enableHighAccuracy to get high accuracy location (working on mobile device)
    var options = {
        enableHighAccuracy: true,
        timeout: 5000,
    };

    function error(err) {

        const location = {
            latitude: -6.914744,
            longitude: 107.609810
        }

        console.log('location', location)

        var latlng = new L.LatLng(location.latitude, location.longitude);
        // set view current position
        var map = L.map('map').setView(latlng, 15)

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        var geocodeService = L.esri.Geocoding.geocodeService();
        var searchControl = L.esri.Geocoding.geosearch().addTo(map);

        // set data map in textbox
        document.getElementById('latitude').value = latlng.lat;
        document.getElementById('longitude').value = latlng.lng;
        document.getElementById('addressInput').value = latlng.address == undefined ? "-" : latlng.address;
        $('#latitude, #longitude, #addressInput').trigger("change");

        // set marker on current location
        var marker = L.marker(latlng).addTo(map);

        // create new layer group for save search result
        var results = new L.LayerGroup().addTo(map);

        // event click for get data on map
        map.on('click', function (e) {
            // geocodeService.reverse() => to get data location as address latitude and longtitude
            geocodeService.reverse().latlng(e.latlng).run(function (error, result) {
                if (error) {
                    return;
                }

                // clear marker on map before create marker
                if (marker) {
                    results.clearLayers();
                    map.removeLayer(marker);
                }

                // to add marker and popup result address
                marker = L.marker(result.latlng).addTo(map).bindPopup(result.address.Match_addr).openPopup();

                document.getElementById('latitude').value = result.latlng.lat;
                document.getElementById('longitude').value = result.latlng.lng;
                document.getElementById('addressInput').value = result.address.Match_addr;
            });
        });

        map.invalidateSize();

        // event get result from search
        searchControl.on('results', function (data) {
            results.clearLayers();
            for (var i = data.results.length - 1; i >= 0; i--) {
                results.addLayer(L.marker(data.results[i].latlng));
                document.getElementById('latitude').value = data.results[i].latlng.lat;
                document.getElementById('longitude').value = data.results[i].latlng.lng;
                document.getElementById('addressInput').value = data.results[i].properties.Match_addr;
            }
        });
    }

    // navigator for get current position
    navigator.geolocation.getCurrentPosition(function (loc) {
        var latlng = new L.LatLng(loc.coords.latitude, loc.coords.longitude);

        console.log('latlng', latlng)

        // set view current position
        var map = L.map('map').setView(latlng, 15)

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        var geocodeService = L.esri.Geocoding.geocodeService();
        var searchControl = L.esri.Geocoding.geosearch().addTo(map);

        // set data map in textbox
        document.getElementById('latitude').value = latlng.lat;
        document.getElementById('longitude').value = latlng.lng;
        document.getElementById('addressInput').value = latlng.address == undefined ? "-" : latlng.address;

        // set marker on current location
        var marker = L.marker(latlng).addTo(map);

        // create new layer group for save search result
        var results = new L.LayerGroup().addTo(map);

        // event click for get data on map
        map.on('click', function (e) {
            // geocodeService.reverse() => to get data location as address latitude and longtitude
            geocodeService.reverse().latlng(e.latlng).run(function (error, result) {
                if (error) {
                    return;
                }

                // clear marker on map before create marker
                if (marker) {
                    results.clearLayers();
                    map.removeLayer(marker);
                }

                // to add marker and popup result address
                marker = L.marker(result.latlng).addTo(map).bindPopup(result.address.Match_addr).openPopup();

                document.getElementById('latitude').value = result.latlng.lat;
                document.getElementById('longitude').value = result.latlng.lng;
                document.getElementById('addressInput').value = result.address.Match_addr;
            });
        });

        // event get result from search
        searchControl.on('results', function (data) {
            results.clearLayers();
            for (var i = data.results.length - 1; i >= 0; i--) {
                results.addLayer(L.marker(data.results[i].latlng));
                document.getElementById('latitude').value = data.results[i].latlng.lat;
                document.getElementById('longitude').value = data.results[i].latlng.lng;
                document.getElementById('addressInput').value = data.results[i].properties.Match_addr;
            }
        });
    }, error, options);
});